FROM openjdk:11-slim

ARG app_name

COPY app/${app_name}/build/libs/${app_name}-0.1.0.jar /app.jar

#ENTRYPOINT ["java", "-jar", "/app.jar"]

